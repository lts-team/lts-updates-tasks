A package is being proposed for EOL (end-of-life) because it can no longer be
supported adequately. Please fill in the information sections below.

## Metadata

- package: (the source package being proposed for EOL)
- releases: (list the releases where the package is present and from which its
  removal is being proposed, e.g., stretch, jessie, etc.)

## Current State

(Describe the current state of the package)

(Consider including information concerning the state of the package in more
recent Debian releases, the current state of upstream development, and any other
details which may be relevant. What are other vendors doing regarding the
specific version proposed to be EOL'ed?)

## Obstacles Preventing Continued Support

(Describe the obstacles to continued support)

(What are the specific issues that cannot be fixed? Are there patches for more
recent versions, and why can they not be backported? Is upstream support already
discontinued or will it be discontinued on a known date?)

## Alternative Courses of Action

(Describe what alternative courses of action have been considered.)

(What are other vendors doing in terms of security support of that specific
version? How feasible is to do a full version backport from a more recent Debian
release? Can the scope of support for the package be limited in a meaningful
way which allows continued support at a reduced level rather than a complete
dropping of support?  Is it possible to ignore a certain number of CVEs and
continue providing support for other vulnerabilities as they become known? What
other alternatives have you considered? Please give details about why a
particular alternative is or is not feasible/preferred.)

## Potential Impacts

### Impacts of taking no action

(Describe the potential impacts of taking no action.)

### Impacts of full EOL

(Describe the impact of dropping support as proposed.)

### Impacts of alternative course(s) of action

(Describe the impacts of the alternative courses of action which have been
proposed or which have been considered and ruled out.)

### Additional impacts

(Describe possible impacts which may reach beyond this specific package, such as
with dependencies, reverse dependencies, third party applications, applications
which users may have developed or deployed against this package, etc.)

/due in 7 days
/assign @roberto @santiago
