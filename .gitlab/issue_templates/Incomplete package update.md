Title: Incomplete update for $SRCPKG (DLA-XXXX-1)


As you may be aware, we are in the process of implementing a
[git workflow] (https://lts-team.pages.debian.net/git-workflow-lts.html) for all
LTS/ELTS uploads to maintain a history of all changes and improve traceability
and overall package quality.

It appears that one of your recent uploads is incomplete:

- Package: $SRCPKG
- Version: $VERSION
- Release date: $DATE
- Advisory: DLA-XXXX-1

The following problems were found:

(NOTE TO SUBMITTER: remove inapplicable items from the list below and remove
this note before finalizing and creating the issue)

- [ ] VCS link is missing in the [package
database](https://gitlab.com/freexian/services/deblts-team/debian-lts/-/blob/master/packages.yml)
- [ ] VCS link is present but points to non-existent repository in the [package
database](https://gitlab.com/freexian/services/deblts-team/debian-lts/-/blob/master/packages.yml)
- [ ] Git tags are missing from the linked VCS
- [ ] DLA missing from mailing list archive

The package can be uploaded to a git repository under the [lts-packages
team](https://salsa.debian.org/lts-team/packages/) or any other Salsa resource,
and the VCS link should then be added to the [package
database](https://gitlab.com/freexian/services/deblts-team/debian-lts/-/blob/master/packages.yml).
A new repository should only be created in the event that working in the
maintainer repository is not possible nor is the creation of a fork possible.
Consult the [git workflow] (https://lts-team.pages.debian.net/git-workflow-lts.html)
page for further details.

If the DLA is missing from the mailing list archive and you think that you sent
it, check to ensure that you GPG-signed the message and that it was addressed to
the correct mailing list (debian-lts-announce@lists.debian.org).

If you need any help or have any questions about this process, don't hesitate to
reach out.

Note: the system monitoring the uploads is currently in test mode, so some false
positive warnings may occur. If you believe that any information in this issue
is incorrect, please let us know.

Best regards,

LTS Administration

/due in 7 days
(SUBMITTER NOTE: remember to assign the issue to the responsible contributor)
