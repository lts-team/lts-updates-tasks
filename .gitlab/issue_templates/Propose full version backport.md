## Metadata

- package: (the source package being proposed for a full backport)
- releases: (list the releases where the full backport is needed)
- status in ELTS releases

## Current State

(Describe the current state of the package)

(Consider including information concerning:
- the state of the package in more recent Debian releases
- the current state of upstream development,
- what are other vendors doing regarding the specific version?
- any other details which may be relevant.)

## Obstacles Preventing Single Patch Backporting

(Describe the obstacles to single patch backporting)

(What are the specific patches that cannot be backported? Is upstream
support already discontinued for the specific major version present in the
target release, or will it be discontinued on a known date?)

## Alternative Courses of Action

(Describe what alternative courses of action have been considered.)

Is it possible to ignore a certain number of CVEs and continue doing
single patch backports? Please give details about why a particular
alternative is or is not feasible/preferred.)

## Potential Impacts

### Impacts of taking no action

(Describe the potential impacts of taking no action.)

### Impacts of alternative course(s) of action

(Describe the impacts of the alternative courses of action which have been
proposed or which have been considered and ruled out.)

### Additional impacts

(Describe possible impacts which may reach beyond this specific package, such as
with dependencies, reverse dependencies, third party applications, applications
which users may have developed or deployed against this package, etc.)

## Backport checklist

- [ ] The backport successfully builds on all the supported architectures.
- [ ] The backport has been fully tested. Please describe how.
- [ ] It has been possible to verify the addressed vulnerabilities are exploitable in the current version.
- [ ] It has been possible to verify the addressed vulnerabilities are fixed by the backport.
- [ ] The reverse dependencies work well with the proposed backport. Please list the reverse dependencies that you have tested.

### Source debdiff against the version being backported

(Please provide the debdiff between the original and the backported versions.
In general, this means the version in the newer release and the backport being
proposed for the target release.)

### Binary debdiff against the current package in the target release

(Please provide the binary debdiff between the current and the backported
package in the target release.)
